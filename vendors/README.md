# Vendors folders with images by SKU
```
Vendor
    VendorName 1
        ProductSKU 1.1
        ProductSKU 1.2
        ProductSKU 1.3
        ...
        VendorCatalog 1.csv
    VendorName 2
        ProductSKU 2.1
        ProductSKU 2.2
        ProductSKU 2.3
        ...
        VendorCatalog 2.csv
    ...
```
