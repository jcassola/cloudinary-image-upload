# Cloudinary Image Upload

## Install & Run

1. Clone project

```bash
git clone https://gitlab.com/ariamfr/cloudinary-image-upload.git
```

2. Move to project folder

```bash
cd cloudinary-image-upload
```

3. Install dependencies

```bash
npm i
```

or

```bash
yarn
```

4. Create a `.env` from `.env.example` and set the environments
   vars [values](https://gitlab.com/front10-devs/flexshopper/flexshopper/-/wikis/Cloudinary-configuration#environment-vars-for-cloudinary-image-upload-project-script)

5. Update `vendorsConfig.json` with the configuration by vendor with this format

```json
[
  {
    // Vendor code to reference folder name inside `vendors` folder
    "code": "vendor-name-folder",
    // Name of the `csv` file
    "file": "Vendor-Catalog.csv",
    // Path inside cloudinary to store the img
    "cloudinaryFolder": "cloudinary/image/path/",
    "workType": "FULL_DOWNLOAD"|"PATH"
  }
]
```

6. Create the folder structure for the vendor to parse inside `vendors` folder with the format:

```bashpro shell script
> Vendors folders structure with images by SKU:

./vendors
  ├── VendorCode 1
  │   ├── ProductSKU 1.1
  │   ├── ProductSKU 1.2
  │   ├── ProductSKU 1.3
  │   │   ...
  │   └── Vendor-Catalog 1.csv
  ├── VendorCode 2
  │   ├── ProductSKU 2.1
  │   ├── ProductSKU 2.2
  │   ├── ProductSKU 2.3
  │   │   ...
  │   └── Vendor-Catalog 2.csv
  └── ...
```

7. Run the script with

```bashpro shell script
npm run dev
```

or

```bashpro shell script
yarn dev
```

# FIXTURES

## Error: libjpeg error: Unsupported color conversion request

You must install the library [ImageMagick](https://imagemagick.org/script/download.php) and check the type of the image's encoded colors:

```bash
identify -format '%[colorspace]' vendors/[name]/source/[image_source]
```

If the encoded is CMYK then you must convert the image manually
because the used library does not support that encoded
